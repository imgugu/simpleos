<h>simpleos</h>
========
<p>是一个web开发框架，里面内置资讯，博客，论坛，文档已经网站的组织架构等功能模块，开发简单，可以直接部署使用。</p>
<p>相关引用的jar文件，到官网上面下载<a href="http://www.simpleos.net">http://www.simpleos.net</a></p>
<p>部分图例</p>
<img style='width:600px;' src='http://www.simpleos.net/app/doc/d1/images/1.2.1.png'>
<img style='width:600px;' src='http://www.simpleos.net/app/doc/d1/images/1.2.8.png'>